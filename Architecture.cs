﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Generics.Robots
{
    public abstract class RobotAI
    {
        public abstract IMoveCommand GetCommand();
    }

    public class ShooterAI : RobotAI
    {
        int counter = 1;

        public override IMoveCommand GetCommand()
        {
            return ShooterCommand.ForCounter(counter++);
        }
    }

    public class BuilderAI : RobotAI
    {
        int counter = 1;
        public override IMoveCommand GetCommand()
        {
            return BuilderCommand.ForCounter(counter++);
        }
    }

    public abstract class Device
    {
        public abstract string ExecuteCommand(IMoveCommand command);
    }

    public class Mover : Device
    {
        public override string ExecuteCommand(IMoveCommand command)
        {
            if (command == null)
                throw new ArgumentException();
            return $"MOV {command.Destination.X}, {command.Destination.Y}";
        }
    }


    public interface IRobot
    {
        IEnumerable<string> Start(int steps);
    }

    public class Robot<T> : IRobot where T : RobotAI
    {
        T ai;
        Device device;

        public Robot(T ai, Device executor)
        {
            this.ai = ai;
            this.device = executor;
        }

        public IEnumerable<string> Start(int steps)
        {
             for (int i=0;i<steps;i++)
            {
                var command = ai.GetCommand();
                if (command == null)
                    break;
                yield return device.ExecuteCommand(command);
            }
        }

        // Тот же вопрос: не вызывается без указания типа 
        // Robot.Create(new BuilderAI(), new Mover()); не работает без Robot<...> 
        // через IRobot пытался решить проблему, наверное, он тут не нужен
        public static IRobot Create<T1>(T1 ai, Device executor) where T1: RobotAI
        {
            return new Robot<T1>(ai, executor);
        }
    }
    

}
